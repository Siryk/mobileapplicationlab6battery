package com.example.battery;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private int batteryLevel = 50;
    private ImageView imageView;
    private TextView batteryLevelTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);
        batteryLevelTextView = findViewById(R.id.batteryLevelTextView);
        Button increaseButton = findViewById(R.id.increaseButton);
        Button decreaseButton = findViewById(R.id.decreaseButton);

        updateBatteryLevel();

        increaseButton.setOnClickListener(v -> increaseBatteryLevel());
        decreaseButton.setOnClickListener(v -> decreaseBatteryLevel());
    }

    private void increaseBatteryLevel() {
        if (batteryLevel < 100) {
            batteryLevel += 10;
            updateBatteryLevel();
        }
    }

    private void decreaseBatteryLevel() {
        if (batteryLevel > 0) {
            batteryLevel -= 10;
            updateBatteryLevel();
        }
    }

    private void updateBatteryLevel() {
        imageView.setImageLevel(batteryLevel);
        batteryLevelTextView.setText(batteryLevel + "%");
    }
}